---
version: "3.8"

services:
  traefik:
    image: traefik:${TRAEFIK_VERSION:?err}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_traefik"
    restart: ${CONTAINER_RESTART:-unless-stopped}
    depends_on:
      - docker-socket-proxy
      - wordpress
    networks:
      - docker-socket-proxy
      - webserver
    ports:
      - "80:80"
      - "443:443"
      - "127.0.0.1:${TRAEFIK_API_PORT:-8079}:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
      - "./traefik/traefik-dynamic-config.toml:/traefik-dynamic-config.toml"
    command:
      - "--global.sendAnonymousUsage=false"
      - "--log.level=${TRAEFIK_LOG_LEVEL:-ERROR}"
      - "--api=${TRAEFIK_API_ENABLED:-false}"
      - "--api.insecure=true"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web.http.redirections.entrypoint.to=websecure"
      - "--entrypoints.web.http.redirections.entrypoint.scheme=https"
      - "--entrypoints.web.http.redirections.entrypoint.permanent=true"
      - "--entrypoints.websecure.address=:443"
      - "--certificatesresolvers.route53.acme.dnschallenge=true"
      - "--certificatesresolvers.route53.acme.dnschallenge.provider=route53"
      - "--certificatesresolvers.route53.acme.storage=/letsencrypt/route53.json"
      - "--providers.file.filename=/traefik-dynamic-config.toml"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.endpoint=tcp://docker-socket-proxy:2375"
      - "--providers.docker.network=docker-socket-proxy"
    env_file:
      - .env

  docker-socket-proxy:
    image: tecnativa/docker-socket-proxy:${DOCKERPROXY_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_docker-socket-proxy"
    restart: unless-stopped
    networks:
      - docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro,delegated
    environment:
      CONTAINERS: 1

  wordpress:
    image: wordpress
    user: 1000:1000
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_wordpress"
    restart: always
    environment:
      WORDPRESS_DB_HOST: "db:3306"
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_NAME: wordpress
      WORDPRESS_DB_PASSWORD: "${MYSQL_PASSWORD:?err}"
    env_file:
      - .env
    volumes:
      - wordpress:/var/www/html
      - /data/uploads:/var/www/html/wp-content/uploads
    networks:
      - webserver
      - database
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=${COMPOSE_PROJECT_NAME:-composeproject}_webserver"
      - "traefik.http.middlewares.wpadmin-ipwhitelist.ipwhitelist.sourcerange=${WPADMIN_WHITELIST:-0.0.0.0/0}"
      - "traefik.http.middlewares.limiter.ratelimit.average=${RATE_LIMIT_AVG:-10}"
      - "traefik.http.middlewares.limiter.ratelimit.burst=${RATE_LIMIT_BURST:-20}"
      - "traefik.http.middlewares.naked.redirectregex.regex=^https://${FQDN}/(.*)"
      - "traefik.http.middlewares.naked.redirectregex.replacement=https://${FQDN_PREFIX:-www}.${FQDN:?err}/$${1}"
      - "traefik.http.middlewares.naked.redirectregex.permanent=true"
      - "traefik.http.routers.wordpress.entrypoints=websecure"
      - "traefik.http.routers.wordpress.rule=Host(`${FQDN:?err}`) || Host(`${FQDN_PREFIX:-www}.${FQDN:?err}`)"
      - "traefik.http.routers.wordpress.tls.certresolver=route53"
      - "traefik.http.routers.wordpress.middlewares=limiter@docker"
      - "traefik.http.routers.wpadmin.entrypoints=websecure"
      - "traefik.http.routers.wpadmin.rule=((Host(`${FQDN:?err}`) || Host(`${FQDN_PREFIX:-www}.${FQDN:?err}`)) && PathPrefix(`/wp-login`, `/wp-admin`))"
      - "traefik.http.routers.wpadmin.tls.certresolver=route53"
      - "traefik.http.routers.wpadmin.middlewares=wpadmin-ipwhitelist@docker"

  wpcli:
    image: wordpress:cli
    command: tail -f /dev/null
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_wpcli"
    depends_on:
      - wordpress
    user: 1000:1000
    volumes:
      - wordpress:/var/www/html
      - /data/backup:/backup
    networks:
      - webserver
      - database
    environment:
      WORDPRESS_DB_HOST: "db:3306"
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: "${MYSQL_PASSWORD:?err}"
      WORDPRESS_DB_NAME: wordpress

  db:
    image: mariadb:10.10
    command: '--default-authentication-plugin=mysql_native_password'
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_db"
    restart: always
    volumes:
      - db:/var/lib/mysql
    networks:
      - database
    environment:
      MYSQL_USER: wordpress
      MYSQL_DATABASE: wordpress
    env_file:
      - .env

  redis:
    image: redis:7-alpine
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_redis"
    restart: always
    networks:
      - database

networks:
  docker-socket-proxy:
    internal: true
  webserver:
    internal: false
  database:
    internal: true

volumes:
  db:
  wordpress:
...
